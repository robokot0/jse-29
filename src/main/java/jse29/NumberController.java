package jse29;

import java.util.Scanner;

public class NumberController {
    /**
     * Сервисный класс
     */
    NumberService numberService;
    /**
     * объект для ввода комманд
     */
    protected final Scanner scanner = new Scanner(System.in);

    /**
     * Конструктор
     * @param numberService
     */
    public NumberController(NumberService numberService) {
        this.numberService = numberService;
    }

    /**
     * Ввод строкового параметра
     * @param parameterName Имя параметра
     * @return Введенное значение
     */
    public String enterStringCommandParameter(final String parameterName){
        System.out.println("Введите "+parameterName+": ");
        final String parametervalue = scanner.nextLine();
        if (parametervalue.isEmpty())
            return null;
        return parametervalue;
    }
    /**
     * Вычисление суммы двух целых чисел
     * @return 0 выполнено
     * @throws IllegalArgumentException
     */
    public int calcSum() throws IllegalArgumentException {
        System.out.println("[Вычисление суммы двух целых чисел]");
        final String arg1 = enterStringCommandParameter("первый аргумент");
        if(arg1==null) throw new IllegalArgumentException("Ошибка аргумент 1 не задан");
        final String arg2 = enterStringCommandParameter("второй аргумент");
        if(arg2==null) throw new IllegalArgumentException("Ошибка аргумент 2 не задан");
        System.out.println("Сумма: " + Long.toString(numberService.calcSum(arg1,arg2)));
        System.out.println("[ОК]");
        return 0;
    }
    /**
     * Вычисление факториала неотрицательного целого числа
     * @return 0 выполнено
     * @throws IllegalArgumentException
     */
    public int calcFactorial() throws IllegalArgumentException {
        System.out.println("[Вычисление факториала неотрицательного целого числа]");
        final String arg = enterStringCommandParameter("неотрицательное целое число");
        if(arg==null) throw new IllegalArgumentException("Ошибка аргумент не задан");
        System.out.println("Факториал: " + Long.toString(numberService.calcFactorial(arg)));
        System.out.println("[ОК]");
        return 0;
    }
    /**
     * Разложение на сумму чисел Фибоначи
     * @return 0 выполнено
     * @throws IllegalArgumentException
     */
    public int decomposeFibonacci() throws IllegalArgumentException {
        System.out.println("[Вычисление факториала неотрицательного целого числа]");
        final String arg = enterStringCommandParameter("неотрицательное целое число");
        if(arg==null) throw new IllegalArgumentException("Ошибка аргумент не задан");
        long[] result = numberService.decomposeFibonacci(arg);
        System.out.println("Разложение:");
        for(int i=0;i<result.length;i++) {
            System.out.println(result[i]);
        }
        System.out.println("[ОК]");
        return 0;
    }
    /**
     * Показать справку
     * @return 0
     */
    public int displayHelp()  {
        System.out.println("sum - Сумма двух целых чисел");
        System.out.println("fact - Факториал числа");
        System.out.println("fib - Разложение по числам Фибоначи");
        System.out.println("exit - Выход");
        System.out.println("help - Справка");
        return 0;
    }
    /**
     * Показать приглашешние
     * @return 0
     */
    public int displayWelcome()  {
        System.out.println("Введите комманду");
        return 0;
    }
    /**
     * Показатьошибку
     * @return
     */
    public int displayError() {
        System.out.println("Неизвестная комманда");
        return 0;
    }
}
