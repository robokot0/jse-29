package jse29;

import com.google.common.math.BigIntegerMath;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Сервисный класс
 */
public class NumberService {
    /**
     * Строку в BigInteger
     * @param arg строка
     * @return BigInteger
     * @throws IllegalArgumentException если по строке не удалось получить число
     */
    public BigInteger convertStringToBigInteger(String arg) throws IllegalArgumentException {
        BigInteger bigIntegerArg = null;
        try {
            bigIntegerArg = new BigInteger(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("По строке " + arg + " не удалось определить число");
        }
        return bigIntegerArg;
    }
    /**
     * Проверка что BigInteger arg можно преобразоать в long без потерь
     * @param arg BigInteger
     * @return long преобразованный из BigInteger arg
     * @throws IllegalArgumentException если long не удается преобразовать без потерь в BigInteger
     */

    public long testBigIntegerAsLong(BigInteger arg) throws IllegalArgumentException {
        if (arg.compareTo(BigInteger.valueOf(Long.MIN_VALUE))==-1){
            throw new IllegalArgumentException(arg.toString() + " меньше чем допустимо для типа long");
        }
        if (arg.compareTo(BigInteger.valueOf(Long.MAX_VALUE))==1){
            throw new IllegalArgumentException(arg.toString() + " больше чем допустимо для типа long");
        }
        return arg.longValue();
    }

    /**
     * Вычисление суммы двух целых чисел
     * @param arg1 число 1 задается строкой
     * @param arg2 число 2 задается строкой
     * @return Сумма
     * @throws IllegalArgumentException если из строк не удается получить числа или результат не удается поместить
     * в переменную типа long
     */
    public long calcSum(String arg1, String arg2) throws IllegalArgumentException {
        BigInteger bigIntegerArg1 = convertStringToBigInteger(arg1);
        BigInteger bigIntegerArg2 = convertStringToBigInteger(arg2);
        testBigIntegerAsLong(bigIntegerArg1);
        testBigIntegerAsLong(bigIntegerArg2);
        BigInteger result = bigIntegerArg1.add(bigIntegerArg2);
        try {
            testBigIntegerAsLong(result);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Сумма " + e.getMessage());
        }
        return result.longValue();
    }

    /**
     * Посчитать факториал
     * @param arg число в виде строки от которого считать факториал
     * @return значение факториала
     * @throws IllegalArgumentException если из строки не удается получить число или если число меньше нуля или
     * если результат не удается поместить в переменную типа long
     */
    public Long calcFactorial(String arg) throws IllegalArgumentException {
        Long longArg = testBigIntegerAsLong(convertStringToBigInteger(arg));
        if (longArg<0) {
            throw new IllegalArgumentException(arg + " должен быть больше 0 или 0");
        }
        Long factorialValue = 1l;
        try {
            for (Long i = 1l; i <= longArg; i++) {
                factorialValue = Math.multiplyExact(i,factorialValue);
            }
        } catch (ArithmeticException e) {
            throw new IllegalArgumentException("Факториал от "+arg+" не может быть вычислен c использованием Long");
        }
        return factorialValue;
    }

    /**
     * Рассчитать числа Фиббоначи не большие чем аргумент
     * @param longArg аргумент
     * @return ArrayList<Long> с числами Фиббоначи
     */
    public ArrayList<Long> calcFibonacci(Long longArg){
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(0,1l);
        arrayList.add(1,1l);
        Integer index = 2;
        Long indexFib = 2l;
        while(indexFib<=longArg){
            arrayList.add(index,indexFib);
            index++;
            indexFib=arrayList.get(index-1)+arrayList.get(index-2);
        }
        return arrayList;
    }
    /**
     * Раскладывает неотрицательное целое число на сумму чисел Фибоначчи
     * @param arg неотрицательное целое число
     * @return массив long чисел Фибоначчи которые должны давать в сумме arg
     * @throws IllegalArgumentException если arg не удается преобразовать в целое число или arg меньше 0 или arg
     * не удается представить в виде суммы чисел из ряда Фибоначчи (видимо это только 0)
     */
    public long[] decomposeFibonacci(String arg) throws IllegalArgumentException {
        Long longArg = testBigIntegerAsLong(convertStringToBigInteger(arg));
        if (longArg<0) {
            throw new IllegalArgumentException(arg + " должен быть больше либо равен 0");
        }
        ArrayList<Long> arrayList =  calcFibonacci(longArg);
        Long tekArg = longArg;
        Integer index = arrayList.size()-1;
        while (tekArg>0 && index>=0) {
            if(arrayList.get(index)>tekArg){
                arrayList.set(index,0l);
            } else {
                tekArg=tekArg-arrayList.get(index);
            }
            index--;
        }
        if(tekArg==0 && index>=0){
            for(int i=index;i>=0;i--){
                arrayList.set(i,0l);
            }
        }
        arrayList.removeIf(a ->  (a.equals(0l)));
        if(tekArg>0 || arrayList.size()==0){
            throw new IllegalArgumentException(arg + " не удалось представить как сумму чисел Фибоначчи остаток "+tekArg.toString());
        }
        long[] result = new long[arrayList.size()];
        arrayList.forEach((Long a)->{result[arrayList.indexOf(a)]=a.longValue();});
        return result;
    }
}
