package jse29;

public class TerminalConst {
    public static final String SUM = "sum";
    public static final String FACTORIAL = "fact";
    public static final String FIBONACCCI = "fib";
    public static final String EXIT = "exit";
    public static final String HELP = "help";
}
